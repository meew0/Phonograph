# Phonograph
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://gitlab.com/meew0/Phonograph/blob/master/LICENSE.txt)

**A material designed local music player for Android.**

![Screenshots](./art/art.jpg?raw=true)

This project is a fork of Karim Abou Zeid's [Phonograph](https://github.com/kabouzeid/Phonograph/). It is the result of my search for an audio player that:

1. looks good,
2. supports obscure file formats (most notably Ogg Opus)
3. is free and contains no ads.

As I have found no such player myself, I decided to amend the original Phonograph (which matches criteria 1 and 3) by adding a native code playback solution based on FFmpeg. In the current version, it should be mostly functional, with some bugs probably being left. In addition to those features, I also wanted a player which I have full control over and can add or change things as I desire, if I ever find things I dislike in the future.

Every playback feature of the original app should be supported, except for gapless playback. You can see my attempt at implementing it in the git history (`gapless` branch), but I gave up for now because I ran into undebuggable concurrency problems (and likely also a JVM bug). The non-gapless playback is nearly gapless anyway, only if you listen carefully you can hear a slight jump in playback.

If you want to use this, you'll have to compile it yourself for now. Feel free to get into contact with me though (it's probably best to email me at meew0@protonmail.com), if you have any problems or want to report any bugs while using it yourself. If you are Karim Abou Zeid, you are very welcome to merge these changes into upstream if you want, just email me if you have any questions.

Once you have built and installed the app, you have to enable custom playback in the settings. Then, just play whatever you want. Enjoy!