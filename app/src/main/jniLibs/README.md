# FFmpeg

This directory, with its subdirectories, contains precompiled versions of the FFmpeg libraries
(https://ffmpeg.org/). The files themselves have been taken from Ilja Kosynkin's
[Android FFmpeg development kit](https://github.com/IljaKosynkin/FFmpeg-Development-Kit).
They are available under the FFmpeg licences — see LICENSE.md for details.