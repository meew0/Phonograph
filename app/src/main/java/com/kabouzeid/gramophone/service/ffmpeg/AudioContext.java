package com.kabouzeid.gramophone.service.ffmpeg;

import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioTimestamp;
import android.media.AudioTrack;
import android.util.Log;

import java.util.concurrent.locks.ReentrantLock;

public class AudioContext {
    private class TrackReleasedException extends IllegalStateException {}

    private enum State {
        UNINITIALISED(false),
        INITIALISED(true), PLAYING(true), PAUSED(true),
        STOPPED(true), RELEASED(false);

        final boolean trackUsable;
        State(boolean trackUsable) {
            this.trackUsable = trackUsable;
        }

        public boolean isTrackUsable() {
            return trackUsable;
        }
    }

    public static final int NYI = -100, INTERRUPTED = -0xdead1, TRACK_RELEASED = -0xdead2, FINISHED_DECODING = -0xcafe;

    private int error = 0, internalError = 0;
    private String avStrError = "";

    // Pointer to the C struct that contains FFmpeg contexts etc.
    public long internalPtr;

    private int trackDuration, playbackOffset;
    private long totalBytesWritten = 0;

    private AudioTrack track;
    private State state = State.UNINITIALISED;

    // We use a fair ReentrantLock so that the playbackThread cannot obtain the lock over and over
    // when some other action happens at the same time (e.g. stop). Otherwise the UI would appear
    // unresponsive.
    public final ReentrantLock lock = new ReentrantLock(true);

    public AudioContext() {
    }

    // This method is called from native code to setup playback after a file has been loaded
    private void buildTrack(int sampleRate, int numChannels) {
        int channelMask;
        switch(numChannels) {
            // This data is taken from Android's documentation. It's a bit overkill to have this
            // here (who ever uses audio files with 3 channels) but I need to differentiate between
            // mono, stereo, and surround anyway so why not go the entire way
            case 1: channelMask = AudioFormat.CHANNEL_OUT_MONO; break;
            case 2: channelMask = AudioFormat.CHANNEL_OUT_STEREO; break;
            case 3: channelMask = AudioFormat.CHANNEL_OUT_STEREO | AudioFormat.CHANNEL_OUT_FRONT_CENTER; break;
            case 4: channelMask = AudioFormat.CHANNEL_OUT_QUAD; break;
            case 5: channelMask = AudioFormat.CHANNEL_OUT_QUAD | AudioFormat.CHANNEL_OUT_FRONT_CENTER; break;
            case 6: channelMask = AudioFormat.CHANNEL_OUT_5POINT1; break;
            case 7: channelMask = AudioFormat.CHANNEL_OUT_5POINT1 | AudioFormat.CHANNEL_OUT_BACK_CENTER; break;
            case 8: channelMask = AudioFormat.CHANNEL_OUT_7POINT1_SURROUND; break;
            default: channelMask = AudioFormat.CHANNEL_OUT_STEREO;
        }

        this.track = new AudioTrack.Builder()
                .setAudioAttributes(new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .build())
                .setAudioFormat(new AudioFormat.Builder()
                        .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
                        .setSampleRate(sampleRate)
                        .setChannelMask(channelMask)
                        .build())
                .setBufferSizeInBytes(32768)
                .setTransferMode(AudioTrack.MODE_STREAM)
                .build();

        state = State.INITIALISED;
    }

    public void debugState(String info) {
        Log.d("phonograph", "State: " + state + " " + info);
    }

    public void releaseTrack() {
        state = State.RELEASED;
        track.release();
    }

    public void setPositionListener(AudioTrack.OnPlaybackPositionUpdateListener listener) {
        track.setPlaybackPositionUpdateListener(listener);
    }

    public void play() {
        if(!state.isTrackUsable()) throw new IllegalStateException("tried to play() in unusable state " + state);
        state = State.PLAYING;
        track.play();
    }

    public void stop() {
        if(!state.isTrackUsable()) throw new IllegalStateException("tried to stop() in unusable state " + state);
        state = State.STOPPED;
        track.stop();
    }

    public void pause() {
        if(!state.isTrackUsable()) throw new IllegalStateException("tried to pause() in unusable state " + state);
        state = State.PAUSED;
        track.pause();
    }

    public void setVolume(float volume) {
        track.setVolume(volume);
    }

    public boolean getTimestamp(AudioTimestamp timestamp) {
        if(!state.isTrackUsable()) throw new IllegalStateException("tried to getTimestamp() in unusable state " + state);
        return track.getTimestamp(timestamp);
    }

    public int getPlaybackSampleRate() {
        return track.getPlaybackRate();
    }

    public int getPlayState() {
        return track.getPlayState();
    }

    public int getAudioSessionId() {
        return track.getAudioSessionId();
    }

    public int write(byte[] data, int offset, int size) {
        try {
            int written, originalSize = size;
            do {
                written = internalWrite(data, offset, size);
                offset += written;
                size -= written;
            } while (size > 0);
            totalBytesWritten += originalSize;
            return originalSize;
        } catch(InterruptedException e) {
            return INTERRUPTED;
        } catch(TrackReleasedException e) {
            return TRACK_RELEASED;
        }
    }

    public void notifyDecodingFinished() {
        // 2 channels * 2 bytes/sample = 4
        track.setNotificationMarkerPosition((int) (totalBytesWritten / 4));
    }

    public int internalWrite(byte[] data, int offset, int size) throws InterruptedException {
        lock.lockInterruptibly();

        try {
            if(state == State.RELEASED) throw new TrackReleasedException();
            return track.write(data, offset, size, AudioTrack.WRITE_NON_BLOCKING);
        } finally {
            lock.unlock();
        }
    }

    public boolean isErrored() {
        return error != 0;
    }

    public int getErrorCode() {
        return error;
    }

    public int getInternalErrorCode() {
        return internalError;
    }

    public String getAVErrorString() { return avStrError; }

    public int getTrackDuration() {
        return trackDuration;
    }

    public int getPlaybackOffset() {
        return playbackOffset;
    }
}
