package com.kabouzeid.gramophone.service.ffmpeg;

import android.media.AudioTimestamp;
import android.media.AudioTrack;
import android.util.Log;

import java.io.InputStream;

public class FFmpegPlayer {
    private static boolean nativeInitialised = false;

    private boolean cleanedUp = false;
    private final AudioContext ctx;
    private final Thread playbackThread;
    private final long playbackStartNanos;
    private boolean paused = false;

    public FFmpegPlayer(InputStream fileStream, int offsetMs, AudioTrack.OnPlaybackPositionUpdateListener listener) {
        if (!nativeInitialised) {
            init();
            nativeInitialised = true;
        }

        ctx = loadFile(fileStream, offsetMs);
        playbackStartNanos = System.nanoTime();
        ctx.setPositionListener(listener);

        if (ctx.isErrored()) {
            throw new RuntimeException("File loading errored (code = " + ctx.getErrorCode() + ", reason = " + ctx.getAVErrorString() + ")");
        }

        // This thread just needs to call the native method in a loop. We don't need to worry about
        // timing here because the call to AudioTrack#write, which we do synchronously from the
        // native method, blocks until data has been queued for playback
        playbackThread = new Thread(() -> {
            while (!Thread.interrupted()) {
                int result = decodeAndPlayAudio(ctx, ctx.internalPtr);
                if(result < 0) {
                    if(result == AudioContext.INTERRUPTED || result == AudioContext.TRACK_RELEASED || result == AudioContext.FINISHED_DECODING) {
                        break;
                    } else {
                        Log.e("phonographnative", "negative result from decodeAndPlayAudio: " + result);
                    }
                }
            }
        });
        playbackThread.setName("phonographnative-playback");
    }

    static {
        // Load the library we compiled so the native methods actually do something
        System.loadLibrary("phonographnative");
    }

    private native void init();

    private native AudioContext loadFile(InputStream stream, int offsetMs);

    private native int decodeAndPlayAudio(AudioContext ctx, long internalPtr);

    private native void cleanup(AudioContext file);

    private void tryCleanup() {
        ctx.lock.lock();
        try {
            if(cleanedUp) return;

            ctx.releaseTrack();
            cleanup(ctx);
            cleanedUp = true;
        } finally {
            ctx.lock.unlock();
        }
    }

    private void assertNotCleanedUp() {
        if(cleanedUp) throw new IllegalStateException("Player resources already cleaned up");
    }

    @Override
    protected void finalize() throws Throwable {
        tryCleanup();
    }

    public boolean start() {
        ctx.lock.lock();
        try {
            assertNotCleanedUp();
            if(!playbackThread.isAlive()) playbackThread.start();
            ctx.play();
            paused = false;
        } finally {
            ctx.lock.unlock();
        }
        return true;
    }

    public void stop() {
        ctx.lock.lock();
        try {
            ctx.stop();
            if(!playbackThread.isInterrupted()) playbackThread.interrupt();
        } finally {
            ctx.lock.unlock();
        }
    }

    public void release() {
        ctx.lock.lock();
        try {
            stop();
            tryCleanup();
        } finally {
            ctx.lock.unlock();
        }
    }

    public boolean pause() {
        ctx.lock.lock();
        try {
            assertNotCleanedUp();
            ctx.pause();
            return paused = true;
        } finally {
            ctx.lock.unlock();
        }
    }

    public boolean isPaused() {
        return paused;
    }

    public boolean isPlaying() {
        return !isPaused() && playbackThread.isAlive() && ctx.getPlayState() == AudioTrack.PLAYSTATE_PLAYING;
    }

    public int duration() {
        return ctx.getTrackDuration();
    }

    public int position() {
        AudioTimestamp timestamp = new AudioTimestamp();
        int timestampMs = ctx.getPlaybackOffset();
        if (ctx.getTimestamp(timestamp)) {
            timestampMs += (int) ((timestamp.framePosition * 1000) / ctx.getPlaybackSampleRate());
        }
        return timestampMs;
    }

    public boolean setVolume(float vol) {
        ctx.setVolume(vol);
        return true;
    }

    public boolean setAudioSessionId(int sessionId) {
        return false;
    }

    public int getAudioSessionId() {
        return ctx.getAudioSessionId();
    }

    public int getPlaybackOffset() {
        return ctx.getPlaybackOffset();
    }
}
