package com.kabouzeid.gramophone.service.ffmpeg;

import android.content.Context;
import android.content.Intent;
import android.media.AudioTrack;
import android.media.audiofx.AudioEffect;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.kabouzeid.gramophone.R;
import com.kabouzeid.gramophone.service.playback.Playback;
import com.kabouzeid.gramophone.util.PreferenceUtil;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class FFmpegMultiPlayer implements Playback, AudioTrack.OnPlaybackPositionUpdateListener {
    private PlaybackCallbacks callbacks;

    private String currentPath;
    private int currentDuration;
    private FFmpegPlayer current, next;
    private boolean initialised = false;

    private final Context ctx;

    public FFmpegMultiPlayer(Context ctx) {
        this.ctx = ctx;

        if(PreferenceUtil.getInstance(ctx).gaplessPlayback()) {
            Toast.makeText(ctx, R.string.gapless_custom_warning, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean setDataSource(String path, long durationMs) {
        currentPath = path;
        currentDuration = (int) durationMs;
        return setDataSourceInternal(currentPath, 0);
    }

    private boolean setDataSourceInternal(String path, int offsetMs) {
        initialised = false;
        try {
            if(current != null) current.release();

            InputStream stream = ctx.getContentResolver().openInputStream(Uri.parse(path));
            if(stream == null) return false;

            current = new FFmpegPlayer(stream, offsetMs, this);
            setNextDataSource(null, -1);
            sendOpenAudioEffectControlSessionIntent();
            initialised = true;
        } catch(RuntimeException | FileNotFoundException e) {
            e.printStackTrace();
        }

        return initialised;
    }

    /**
     * Copied from MultiPlayer
     */
    private void sendOpenAudioEffectControlSessionIntent() {
        if(getAudioSessionId() == -1) return;

        final Intent intent = new Intent(AudioEffect.ACTION_OPEN_AUDIO_EFFECT_CONTROL_SESSION);
        intent.putExtra(AudioEffect.EXTRA_AUDIO_SESSION, getAudioSessionId());
        intent.putExtra(AudioEffect.EXTRA_PACKAGE_NAME, ctx.getPackageName());
        intent.putExtra(AudioEffect.EXTRA_CONTENT_TYPE, AudioEffect.CONTENT_TYPE_MUSIC);
        ctx.sendBroadcast(intent);
    }

    @Override
    public void setNextDataSource(@Nullable String path, long durationMs) {
        // TODO: implement this. According to my investigation of the MultiPlayer implementation it seems like it does nothing unless gapless playback is activated
    }

    @Override
    public void setCallbacks(PlaybackCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public boolean isInitialized() {
        return initialised;
    }

    @Override
    public boolean start() {
        return current.start();
    }

    @Override
    public void stop() {
        current.stop();
        initialised = false;
    }

    @Override
    public void release() {
        stop();
        current.release();
        if(next != null) next.release();
    }

    @Override
    public boolean pause() {
        return current.pause();
    }

    @Override
    public boolean isPlaying() {
        return initialised && current.isPlaying();
    }

    @Override
    public int duration() {
        if(!initialised) return -1;
        int rawDuration = current.duration();
        if(rawDuration == Integer.MIN_VALUE) {
            // Duration could not be determined by FFmpeg. Use the one determined by Phonograph's
            // media scanner
            return currentDuration;
        } else return rawDuration;
    }

    @Override
    public int position() {
        if(!initialised) return -1;
        return current.position();
    }

    @Override
    public int seek(int whereto) {
        boolean currentlyPlaying = isPlaying();

        // It is infeasible to implement seeking within a running FFmpegPlayer, because the
        // AudioTrack (responsible for playback) has no seeking functionality and the playbackThread
        // (responsible for decoding etc.) has no concept of time. Furthermore, due to the fact that
        // Android only gives us a regular InputStream for the content URI, it is not trivial to
        // implement bidirectional reading, which may be required for seeking backwards.
        //
        // Instead, we implement seeking by just creating a new player, reloading the file entirely,
        // and skipping to a specified offset.
        if(setDataSourceInternal(currentPath, whereto)) {
            start();
            if(!currentlyPlaying) pause();
            return current.getPlaybackOffset();
        }
        return -1; // if reloading the file failed somehow
    }

    @Override
    public boolean setVolume(float vol) {
        return current.setVolume(vol);
    }

    @Override
    public boolean setAudioSessionId(int sessionId) {
        return current.setAudioSessionId(sessionId);
    }

    @Override
    public int getAudioSessionId() {
        if(current != null) return current.getAudioSessionId();
        else return -1;
    }

    @Override
    public void onMarkerReached(AudioTrack track) {
        // The song finished playing
        // TODO: Gapless playback
        initialised = false;
        current.release();
        current = null;
        callbacks.onTrackEnded();
    }

    @Override
    public void onPeriodicNotification(AudioTrack track) {
        // Nothing to do
    }
}
