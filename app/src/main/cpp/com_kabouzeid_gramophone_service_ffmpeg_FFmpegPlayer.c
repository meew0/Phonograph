#include <jni.h>

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavformat/avio.h>
#include <libavutil/avutil.h>
#include <libavutil/opt.h>
#include <libswresample/swresample.h>

#include "com_kabouzeid_gramophone_service_ffmpeg_FFmpegPlayer.h"

#define class_AudioContext (*env)->FindClass(env, "com/kabouzeid/gramophone/service/ffmpeg/AudioContext")
#define class_InputStream (*env)->FindClass(env, "java/io/InputStream")

#define SAMPLE_BUFFER_SIZE 4096
#define OUTPUT_SAMPLE_FORMAT AV_SAMPLE_FMT_S16

// Returned from decodeAndPlayAudio when there is nothing left to decode and play
#define FINISHED_DECODING (-0xcafe)

static JavaVM *vm;

// init
// This method is guaranteed to be called before any other methods, so it is used to perform
// general initialisation tasks.
JNIEXPORT void JNICALL
Java_com_kabouzeid_gramophone_service_ffmpeg_FFmpegPlayer_init(JNIEnv *env, jobject thisObject) {
    // Set the java VM global variable so we can get environments later
    (*env)->GetJavaVM(env, &vm);
}

// Generate an AudioContext that represents that an error occurred.
static jobject generate_errored_context(JNIEnv *env, int code, int internalCode) {
    jclass AudioContext = class_AudioContext;
    jobject ctx = (*env)->NewObject(env, AudioContext,
                                    (*env)->GetMethodID(env, AudioContext, "<init>", "()V"));
    (*env)->SetIntField(env, ctx, (*env)->GetFieldID(env, AudioContext, "error", "I"), code);
    (*env)->SetIntField(env, ctx, (*env)->GetFieldID(env, AudioContext, "internalError", "I"),
                        internalCode);

    // If there is no detail information, simply return the context as is
    if (internalCode == 0) return ctx;

    // Else: Get a string describing the error that occurred
    char errbuf[50];
    av_strerror(internalCode, errbuf, sizeof(errbuf));
    (*env)->SetObjectField(env, ctx, (*env)->GetFieldID(env, AudioContext, "avStrError",
                                                        "Ljava/lang/String;"),
                           (*env)->NewStringUTF(env, errbuf));
    return ctx;
}

// Simply contains an object representing an input stream, for using as the opaque pointer for avio
struct stream_container {
    jobject stream;
};

// Contains all the stuff that needs to be shared across native methods. A pointer to this is stored
// in the Java AudioContext
struct internal_context {
    AVFormatContext *fmt_ctx;
    AVCodecContext *dec_ctx;
    SwrContext *swr_ctx;
    uint8_t **pcm_buf;
    int64_t max_samples;
};

// Callback for avio — this is called when ffmpeg wants to read data
static int read_from_stream(void *opaque, uint8_t *buf, int buf_size) {
    struct stream_container *container = (struct stream_container *) opaque;

    // We are in a different thread from when this was created, so we cannot share the env in the
    // opaque pointer. So we use the VM to get a new env for this thread
    JNIEnv *env;
    (*vm)->GetEnv(vm, &env, JNI_VERSION_1_6);

    // read(byte[])
    jbyteArray jbuf = (*env)->NewByteArray(env, buf_size);
    jclass InputStream = class_InputStream;
    jint bytes_read = (*env)->CallIntMethod(env, container->stream,
                                            (*env)->GetMethodID(env, InputStream, "read", "([B)I"),
                                            jbuf);

    buf_size = FFMIN(buf_size, bytes_read);
    if (buf_size < 0) {
        buf_size = AVERROR_EOF;
        goto end;
    }

    jbyte *elements = (*env)->GetByteArrayElements(env, jbuf, NULL);
    memcpy(buf, elements, buf_size);
    (*env)->ReleaseByteArrayElements(env, jbuf, elements, 0);

    end:
    (*env)->DeleteLocalRef(env, jbuf);
    (*env)->DeleteLocalRef(env, InputStream);

    return buf_size;
}

int64_t
skip_frames(AVFormatContext *fmt_ctx, int64_t stream_ts_min) {
    AVPacket packet;
    av_init_packet(&packet);
    packet.data = NULL;
    packet.size = 0;

    int64_t timestamp = 0;

    do {
        if (av_read_frame(fmt_ctx, &packet) < 0) break;
        timestamp = packet.pts;
        if (timestamp == AV_NOPTS_VALUE) timestamp = packet.dts;
    } while (timestamp < stream_ts_min);

    av_packet_unref(&packet);

    return timestamp;
}

// loadFile
JNIEXPORT jobject JNICALL
Java_com_kabouzeid_gramophone_service_ffmpeg_FFmpegPlayer_loadFile(JNIEnv *env, jobject thisObject,
                                                                   jobject stream, jint offsetMs) {
    AVFormatContext *fmt_ctx = NULL;
    AVIOContext *avio_ctx = NULL;
    uint8_t *avio_ctx_buffer = NULL;
    int error = 0;

    // Create a global reference for the InputStream. This means that even if it will stop being
    // referenced in Java code (which it will, because it's a local variable of
    // FFmpegMultiPlayer#setDataSource), it won't be garbage collected and we can still use it
    jobject global_stream_ref = (*env)->NewGlobalRef(env, stream);
    struct stream_container *container = malloc(sizeof *container);
    container->stream = global_stream_ref;

    fmt_ctx = avformat_alloc_context();

    // Setup avio, so FFmpeg can read from the InputStream.
    //
    // The reason we do it like this (instead of making it read from a file directly) is because
    // Android really wants us to use its "content URIs" instead of turning them into file paths,
    // and you can only really get streams for this.
    // As a positive side effect, this means that if Phonograph ever supports playing from e.g. web
    // sources, we will be prepared for that
    avio_ctx_buffer = av_malloc(4096);
    avio_ctx = avio_alloc_context(avio_ctx_buffer, 4096, 0, container, &read_from_stream, NULL,
                                  NULL);
    fmt_ctx->pb = avio_ctx;

    if ((error = avformat_open_input(&fmt_ctx, NULL, NULL, NULL)) != 0) {
        return generate_errored_context(env, 1, error);
    }

    if ((error = avformat_find_stream_info(fmt_ctx, NULL)) != 0) {
        return generate_errored_context(env, 2, error);
    }

    // Select the best audio stream in the source file
    AVCodec *dec = NULL;
    if ((error = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_AUDIO, -1, -1, &dec, 0))
        < 0) { // if > 0, then the call was successful and ret contains the stream index
        return generate_errored_context(env, 3, error);
    }
    int stream_index = error;
    AVStream *av_stream = fmt_ctx->streams[stream_index];

    // Setup and open decoding
    AVCodecContext *dec_ctx = avcodec_alloc_context3(dec);
    if ((error = avcodec_parameters_to_context(dec_ctx, av_stream->codecpar)) < 0) {
        return generate_errored_context(env, 4, error);
    }

    if ((error = avcodec_open2(dec_ctx, dec, NULL)) < 0) {
        return generate_errored_context(env, 5, error);
    }

    // Setup resampling, because the decoders are likely to give us some strange sample format that
    // Android's AudioTrack does not understand
    SwrContext *swr_ctx = swr_alloc();
    av_opt_set_int(swr_ctx, "in_channel_layout", dec_ctx->channel_layout, 0);
    av_opt_set_int(swr_ctx, "in_sample_rate", dec_ctx->sample_rate, 0);
    av_opt_set_sample_fmt(swr_ctx, "in_sample_fmt", dec_ctx->sample_fmt, 0);
    av_opt_set_int(swr_ctx, "out_channel_layout", dec_ctx->channel_layout, 0);
    av_opt_set_int(swr_ctx, "out_sample_rate", dec_ctx->sample_rate, 0);
    av_opt_set_sample_fmt(swr_ctx, "out_sample_fmt", OUTPUT_SAMPLE_FORMAT, 0);
    if ((error = swr_init(swr_ctx)) < 0) {
        return generate_errored_context(env, 6, error);
    }

    // Allocate space for the PCM buffer. It is 1024 samples by default which should be enough
    uint8_t **pcm_buf = NULL;
    int num_channels = av_get_channel_layout_nb_channels(dec_ctx->channel_layout);
    if ((error = av_samples_alloc_array_and_samples(&pcm_buf, NULL, num_channels,
                                                    SAMPLE_BUFFER_SIZE, OUTPUT_SAMPLE_FORMAT, 0)) <
        0) {
        return generate_errored_context(env, 7, error);
    }

    struct internal_context *internal_ctx = malloc(sizeof *internal_ctx);
    internal_ctx->dec_ctx = dec_ctx;
    internal_ctx->fmt_ctx = fmt_ctx;
    internal_ctx->swr_ctx = swr_ctx;
    internal_ctx->pcm_buf = pcm_buf;

    internal_ctx->max_samples = SAMPLE_BUFFER_SIZE;

    // Create the contexts, and set the field for the internal pointer to that pointer's address
    jclass AudioContext = class_AudioContext;
    jobject ctx = (*env)->NewObject(env, AudioContext,
                                    (*env)->GetMethodID(env, AudioContext, "<init>", "()V"));
    (*env)->SetLongField(env, ctx, (*env)->GetFieldID(env, AudioContext, "internalPtr", "J"),
                         (long) internal_ctx);

    // Determine track duration
    AVRational ms_time_base = av_make_q(1, 1000);
    if (av_stream->duration != AV_NOPTS_VALUE) {
        int64_t duration_ms = av_rescale_q(av_stream->duration, av_stream->time_base, ms_time_base);
        (*env)->SetIntField(env, ctx, (*env)->GetFieldID(env, AudioContext, "trackDuration", "I"),
                            (int) duration_ms);
    } else {
        (*env)->SetIntField(env, ctx, (*env)->GetFieldID(env, AudioContext, "trackDuration", "I"),
                            INT32_MIN);
    }

    // Seek to the specified offset, if one was given
    if (offsetMs > 0) {
        int64_t av_offset = av_rescale_q(offsetMs, ms_time_base, av_stream->time_base);
        int64_t actual_offset = skip_frames(fmt_ctx, av_offset);

        // Calculate where we actually are now, so we know where to offset the playback counter from
        int64_t actual_ms = av_rescale_q(actual_offset, av_stream->time_base, ms_time_base);
        (*env)->SetIntField(env, ctx, (*env)->GetFieldID(env, AudioContext, "playbackOffset", "I"),
                            (int) actual_ms);
    }


    (*env)->CallVoidMethod(env, ctx, (*env)->GetMethodID(env, AudioContext, "buildTrack", "(II)V"),
                           dec_ctx->sample_rate,
                           av_get_channel_layout_nb_channels(dec_ctx->channel_layout));

    return ctx;
}

static int
decode_and_resample_audio_packet(AVCodecContext *dec_ctx, SwrContext *swr_ctx, uint8_t **pcm_buf,
                                 AVFrame *frame, AVPacket packet, int *has_frame, int *decoded,
                                 int64_t *max_samples) {
    *has_frame = 0;
    int num_channels = av_get_channel_layout_nb_channels(dec_ctx->channel_layout);

    // Decode...
    int result = avcodec_decode_audio4(dec_ctx, frame, has_frame, &packet);
    if (result < 0) return result;

    *decoded = FFMIN(result, packet.size);

    // and resample
    int64_t num_samples_resampled = av_rescale_rnd(
            swr_get_delay(swr_ctx, dec_ctx->sample_rate) + frame->nb_samples, dec_ctx->sample_rate,
            dec_ctx->sample_rate, AV_ROUND_UP);

    if (num_samples_resampled > *max_samples) {
        printf("new sample buffer size %li from %li\n", num_samples_resampled, *max_samples);

        // We would overrun the sample buffer if we continued like this, so instead of making
        // swresample do the buffering, we reallocate the buffer to the new size
        av_freep(&pcm_buf[0]);
        result = av_samples_alloc(pcm_buf, NULL, num_channels, (int) num_samples_resampled,
                                  OUTPUT_SAMPLE_FORMAT, 1);
        if (result < 0) return result;

        *max_samples = num_samples_resampled;
    }

    result = swr_convert(swr_ctx, pcm_buf, (int) num_samples_resampled,
                         (const uint8_t **) frame->extended_data, frame->nb_samples);
    if (result < 0) return result;

    // We return how many bytes of data we got. The information about how many samples we got is
    // returned using the `decoded` out parameter
    return av_samples_get_buffer_size(NULL,
                                      num_channels,
                                      result, OUTPUT_SAMPLE_FORMAT, 1);
}

// Play some PCM audio on the AudioTrack given, using the write method given
static int
play_audio(JNIEnv *env, jobject audioContext, jmethodID write, uint8_t **pcm_buf, int numBytes) {
    if (!pcm_buf || !(*pcm_buf)) {
        printf("null buffer");
        return -1;
    }

    // This is really trivial, just use JNI to call the method with the data from the first channel
    // of the result data (hence pcm_buf[0])
    jbyteArray jbuf = (*env)->NewByteArray(env, numBytes);
    (*env)->SetByteArrayRegion(env, jbuf, 0, numBytes, (const jbyte *) pcm_buf[0]);
    jint result = (*env)->CallIntMethod(env, audioContext, write, jbuf, 0, numBytes);
    return result;
}

// decodeAudio
JNIEXPORT jint JNICALL
Java_com_kabouzeid_gramophone_service_ffmpeg_FFmpegPlayer_decodeAndPlayAudio(JNIEnv *env,
                                                                             jobject thisObject,
                                                                             jobject audioContext,
                                                                             jlong internalPtr) {
    struct internal_context *internal_ctx = (struct internal_context *) internalPtr;

    // Get the class and method we need to call to playback audio later, so we don't have to do this
    // multiple times, as there is a bit of an overhead
    jclass AudioContext = class_AudioContext;
    jmethodID method = (*env)->GetMethodID(env, AudioContext, "write", "([BII)I");

    AVPacket packet;
    av_init_packet(&packet);
    packet.data = NULL;
    packet.size = 0;

    int has_frame = 0, decoded, result;
    AVFrame *frame = av_frame_alloc();

    if (av_read_frame(internal_ctx->fmt_ctx, &packet) >= 0) {
        // We have a frame, decode and play it
        do {
            result = decode_and_resample_audio_packet(internal_ctx->dec_ctx, internal_ctx->swr_ctx,
                                                      internal_ctx->pcm_buf, frame, packet,
                                                      &has_frame, &decoded,
                                                      &internal_ctx->max_samples);
            if (result < 0 || decoded < 0) break;
            result = play_audio(env, audioContext, method, internal_ctx->pcm_buf, result);
            if (result < 0) goto end;

            packet.data += decoded;
            packet.size -= decoded;
        } while (packet.size > 0);

        result = 0;
    } else {
        // Some decoders will only start to give us PCM after a couple frames have already been read
        // into memory. So at the end of decoding we need to "flush" these to get all that's left
        packet.data = NULL;
        packet.size = 0;
        do {
            result = decode_and_resample_audio_packet(internal_ctx->dec_ctx, internal_ctx->swr_ctx,
                                                      internal_ctx->pcm_buf, frame, packet,
                                                      &has_frame, &decoded,
                                                      &internal_ctx->max_samples);
            if (result < 0) break;
            result = play_audio(env, audioContext, method, internal_ctx->pcm_buf, result);
            if (result < 0) goto end;
        } while (has_frame);

        // At this point, we have finished decoding everything. We note this in the context so that
        // the player can know when the sound has finished playing.
        (*env)->CallVoidMethod(env, audioContext,
                               (*env)->GetMethodID(env, AudioContext, "notifyDecodingFinished",
                                                   "()V"));
        result = FINISHED_DECODING;
    }

    end:
    av_packet_unref(&packet);
    av_frame_free(&frame);

    return result;
}

// cleanup
JNIEXPORT void JNICALL
Java_com_kabouzeid_gramophone_service_ffmpeg_FFmpegPlayer_cleanup(JNIEnv *env, jobject thisObject,
                                                                  jobject context) {
    jclass AudioContext = class_AudioContext;
    jlong internalPtr = (*env)->GetLongField(env, context,
                                             (*env)->GetFieldID(env, AudioContext, "internalPtr",
                                                                "J"));
    struct internal_context *internal_ctx = (struct internal_context *) internalPtr;

    if (internal_ctx->pcm_buf) av_freep(&internal_ctx->pcm_buf[0]);
    av_freep(&internal_ctx->pcm_buf);
    swr_free(&internal_ctx->swr_ctx);

    avcodec_free_context(&internal_ctx->dec_ctx);

    AVIOContext *avio_ctx = internal_ctx->fmt_ctx->pb;
    struct stream_container *container = (struct stream_container *) avio_ctx->opaque;

    avformat_close_input(&internal_ctx->fmt_ctx);

    // Close the stream
    jclass InputStream = class_InputStream;
    (*env)->CallVoidMethod(env, container->stream,
                           (*env)->GetMethodID(env, InputStream, "close", "()V"));

    // Delete the global reference so the stream can be garbage collected
    (*env)->DeleteGlobalRef(env, container->stream);
    free(container);

    free(internal_ctx);
}

